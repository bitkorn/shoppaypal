<?php

namespace Bitkorn\ShopPaypal\View\Helper;

use Bitkorn\Trinket\View\Helper\AbstractViewHelper;
use Laminas\View\Model\ViewModel;

/**
 *
 * @author allapow
 */
class PaymentServicePosBrand extends AbstractViewHelper
{

    const TEMPLATE = 'template/paymentServicePosBrandPaypal';

    public function __invoke()
    {
        $viewModel = new ViewModel();
        $viewModel->setTemplate(self::TEMPLATE);

        return $this->getView()->render($viewModel);
    }
}
