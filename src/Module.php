<?php

namespace Bitkorn\ShopPaypal;

use Laminas\ModuleManager\Feature\ViewHelperProviderInterface;

/**
 *
 *
 */
class Module implements ViewHelperProviderInterface
{

    public function getConfig()
    {
        return include __DIR__ . '/../config/module.config.php';
    }

    public function getViewHelperConfig()
    {
        return [
            'factories' => [
                'paymentBrandPaypal' => function (\Laminas\View\HelperPluginManager $hpm) {
                    $helper = new View\Helper\PaymentServiceBrand();
                    return $helper;
                },
                'paymentPosBrandPaypal' => function (\Laminas\View\HelperPluginManager $hpm) {
                    $helper = new View\Helper\PaymentServicePosBrand();
                    return $helper;
                }
            ]
        ];
    }

}
