<?php

namespace Bitkorn\ShopPaypal\Controller;

use Bitkorn\Shop\Controller\AbstractShopController;
use Bitkorn\Shop\Service\Basket\BasketFinalizeService;
use Bitkorn\Shop\Service\Basket\BasketService;
use Bitkorn\Shop\Service\ShopService;
use Laminas\Http\Response;
use Laminas\View\Model\ViewModel;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Transaction;
use PayPal\Api\PaymentExecution;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Rest\ApiContext;

/**
 *
 * @author bitkorn
 */
class IndexController extends AbstractShopController
{

    /**
     * @var array
     */
    protected $shopConfig = [];

    /**
     * @var array
     */
    protected $shopPaypalConfig = [];

    /**
     * @var ShopService
     */
    protected $shopService;

    /**
     * @var BasketFinalizeService
     */
    protected $basketFinalizeService;

    /**
     * @param array $shopConfig
     */
    public function setShopConfig(array $shopConfig): void
    {
        $this->shopConfig = $shopConfig;
    }

    /**
     * @param array $shopPaypalConfig
     */
    public function setShopPaypalConfig(array $shopPaypalConfig): void
    {
        $this->shopPaypalConfig = $shopPaypalConfig;
    }

    /**
     * @param ShopService $shopService
     */
    public function setShopService(ShopService $shopService): void
    {
        $this->shopService = $shopService;
    }

    /**
     * @param BasketFinalizeService $basketFinalizeService
     */
    public function setBasketFinalizeService(BasketFinalizeService $basketFinalizeService): void
    {
        $this->basketFinalizeService = $basketFinalizeService;
    }

    /**
     *
     * @return ViewModel|Response
     */
    public function indexAction()
    {
        if (empty($xshopBasketEntity = $this->basketService->checkBasket()) || empty($xshopBasketEntity->getStorageItems())) {
            return $this->redirect()->toRoute('shop_frontend_article_articles');
        }
        $viewModel = new ViewModel();
        $basketUnique = $xshopBasketEntity->getBasketUnique();
        $viewModel->setVariable('shopBasketUnique', $basketUnique);

        /*
         * http://paypal.github.io/PayPal-PHP-SDK/sample/doc/payments/CreatePaymentUsingPayPal.html
         */
        $payer = new Payer();
        $payer->setPaymentMethod('paypal');

        $paypalDetails = new Details();
        $paypalDetails->setShipping($xshopBasketEntity->getArticleShippingCostsComputed())
            ->setTax($xshopBasketEntity->getArticleTaxTotalSumEnd())
            ->setSubtotal($xshopBasketEntity->getArticlePriceTotalSumEnd() - $xshopBasketEntity->getArticleTaxTotalSumEnd());

        $paypalAmount = new Amount();
        $paypalAmount->setCurrency('EUR')
            ->setTotal($xshopBasketEntity->getTotalCosts())
            ->setDetails($paypalDetails);

        $ppItemList = new ItemList();
        foreach($xshopBasketEntity->getStorageItems() as $item) {
            $ppItem = new Item();
            $ppItem->setName($item['shop_article_name'])
                ->setCurrency('EUR')
                ->setQuantity($item['shop_article_amount'])
                ->setSku($item['shop_article_sku'])
                ->setPrice($item['computed_value_article_price_total_sum_end']);
            $ppItemList->addItem($ppItem);
        }

        $transaction = new Transaction();
        $transaction->setAmount($paypalAmount)
//                ->setItemList($ppItemList)
            ->setDescription($this->shopService->getShopConfigurationValue('payment_header'));

        $uri = $this->getRequest()->getUri();
        $base = sprintf('%s://%s', $uri->getScheme(), $uri->getHost());

        $redirectUrls = new RedirectUrls();
        $redirectUrls->setReturnUrl($base . $this->url()->fromRoute('paypal_redirect', ['success' => 1]))
            ->setCancelUrl($base . $this->url()->fromRoute('paypal_redirect', ['success' => 0]));

        $payment = new Payment();
        $payment->setIntent('sale')
            ->setPayer($payer)
            ->setRedirectUrls($redirectUrls)
            ->setTransactions([$transaction]);

        $apiContext = $this->getApiContect();

        try {
            $payment->create($apiContext);
            if ($this->basketService->setPaymentNumber($payment->getId()) < 0) {
                throw new \Exception('Error while save $this->getShopBasketTable()->setPaymentNumber; $shopBasketUnique: ' . $basketUnique . '; $payment->getId(): ' . $payment->getId());
            }
            $this->basketService->setPaymentMethod('paypal');
            if($this->basketFinalizeService->basketOrdered($basketUnique) < 0) {
                $viewModel->setVariable('success', false);
            }
        } catch (\Exception $ex) {
            $this->logger->err($ex->getMessage());
            $this->logger->err($ex->getFile() . ' at line: ' . $ex->getLine());
            return $this->redirect()->toRoute('shop_frontend_basket_choosepayment');
        }

        $approvalUrl = $payment->getApprovalLink();
        // https://www.sandbox.paypal.com/cgi-bin/webscr?cmd=_express-checkout&token=EC-3T59256798071683J
        $viewModel->setVariable('paypalOutUrl', $approvalUrl);
        $viewModel->setVariable('paymentId', $payment->getId());
        $viewModel->setVariable('success', true);

        return $viewModel;
    }

    /**
     * http://paypal.github.io/PayPal-PHP-SDK/sample/doc/payments/ExecutePayment.html
     *
     * http://attsg3.bitkorn.de/paypal-redirect/1?paymentId=PAY-58969037FJ895934GLA6XYZY&token=EC-2VE66275SP401204Y&PayerID=LWUVXVWJTKER8
     *
     * @return ViewModel|Response
     */
    public function redirectAction()
    {
        $viewModel = new ViewModel();

        $paymentId = $this->params()->fromQuery('paymentId');
        $token = $this->params()->fromQuery('token');
        $payerId = $this->params()->fromQuery('PayerID');

        if (empty($paymentId) || empty($token) || empty($payerId)) {
            return $this->redirect()->toRoute('shop_frontend_article_articles');
        }
        $shopBasket = $this->basketService->getShopBasketByPaymentNumber($paymentId);

        if (empty($shopBasket)) {
            $this->layout()->message = [
                'level' => 'error',
                'text' => 'Es ist ein Fehler aufgetreten. Bitte wenden sie sich mit folgender Payment-Number an den Support: ' . $paymentId
            ];
            return $viewModel;
        }

        $apiContext = $this->getApiContect();
        $payment = Payment::get($paymentId, $apiContext);
        $execution = new PaymentExecution();
        $execution->setPayerId($payerId);

        try {
            $resultPayment = $payment->execute($execution, $apiContext);
            /**
             * @var string $paymentState The state of the payment, authorization, or order transaction. The value is:
             *  - created. The transaction was successfully created.
             *  - approved. The buyer approved the transaction.
             *  - failed. The transaction request failed.
             */
            $paymentState = $resultPayment->getState();
            if ($paymentState == 'approved') {
                $this->basketFinalizeService->basketPayed($shopBasket['shop_basket_unique']);
            } else if ($paymentState == 'failed') {
                $this->basketService->setShopBasketStatusByPaymentNumber($paymentId, 'failed');
            }
            return $this->redirect()->toRoute($this->shopConfig['after_payment_redirect_route'],
                [$this->shopConfig['after_payment_redirect_route_param_name'] => $shopBasket['shop_basket_unique']]);
        } catch (\Exception $ex) {
            $this->logger->err($ex->getMessage());
            $this->logger->err($ex->getFile() . ' at line: ' . $ex->getLine());
        }
        return $viewModel;
    }

    /**
     *
     * @return ViewModel|Response
     */
    public function indexPosAction()
    {
        if (empty($xshopBasketEntity = $this->basketService->checkBasket()) || empty($xshopBasketEntity->getStorageItems())) {
            return $this->redirect()->toRoute('shop_frontend_article_articles');
        }
        $viewModel = new ViewModel();
        $this->layout('layout/admin');

        $viewModel->setVariable('shopBasketUnique', $xshopBasketEntity->getBasketUnique());

        /*
         * http://paypal.github.io/PayPal-PHP-SDK/sample/doc/payments/CreatePaymentUsingPayPal.html
         */
        $payer = new Payer();
        $payer->setPaymentMethod('paypal');

        $paypalDetails = new Details();
        $paypalDetails->setShipping($xshopBasketEntity->getArticleShippingCostsComputed())
            ->setTax($xshopBasketEntity->getArticleTaxTotalSum())
            ->setSubtotal($xshopBasketEntity->getArticlePriceTotalSum() - $xshopBasketEntity->getArticleTaxTotalSum());

        $paypalAmount = new Amount();
        $paypalAmount->setCurrency('EUR')
            ->setTotal($xshopBasketEntity->getTotalCosts())
            ->setDetails($paypalDetails);

        $transaction = new Transaction();
        $transaction->setAmount($paypalAmount)
//                ->setItemList($paypalItemList)
            ->setDescription($this->shopService->getShopConfigurationValue('payment_header'));

        $uri = $this->getRequest()->getUri();
        $base = sprintf('%s://%s', $uri->getScheme(), $uri->getHost());

        $redirectUrls = new RedirectUrls();
        $redirectUrls->setReturnUrl($base . $this->url()->fromRoute('paypal_redirect_pos', ['success' => 1]))
            ->setCancelUrl($base . $this->url()->fromRoute('paypal_redirect_pos', ['success' => 0]));

        $payment = new Payment();
        $payment->setIntent('sale')
            ->setPayer($payer)
            ->setRedirectUrls($redirectUrls)
            ->setTransactions([$transaction]);

        $apiContext = $this->getApiContect();

        try {
            $payment->create($apiContext);
            if ($this->basketService->setPaymentNumber($payment->getId()) < 0) {
                throw new \Exception('Error while save $this->getShopBasketTable()->setPaymentNumber; $shopBasketUnique: ' . $xshopBasketEntity->getBasketUnique() . '; $payment->getId(): ' . $payment->getId());
            }
            $this->basketService->setPaymentMethod('paypal');
            $this->basketFinalizeService->basketOrdered($xshopBasketEntity->getBasketUnique());
        } catch (\Exception $ex) {
            $this->logger->err($ex->getMessage());
            $this->logger->err($ex->getFile() . ' at line: ' . $ex->getLine());
            $this->getResponse()->setStatusCode(\Laminas\Http\PhpEnvironment\Response::STATUS_CODE_500);
        }

        $approvalUrl = $payment->getApprovalLink();
        // https://www.sandbox.paypal.com/cgi-bin/webscr?cmd=_express-checkout&token=EC-3T59256798071683J
        $viewModel->setVariable('paypalOutUrl', $approvalUrl);
        $viewModel->setVariable('paymentId', $payment->getId());

        if ($approvalUrl) {
            return $this->redirect()->toUrl($approvalUrl);
        }

        return $viewModel;
    }

    /**
     * http://paypal.github.io/PayPal-PHP-SDK/sample/doc/payments/ExecutePayment.html
     *
     * http://attsg3.bitkorn.de/paypal-redirect/1?paymentId=PAY-58969037FJ895934GLA6XYZY&token=EC-2VE66275SP401204Y&PayerID=LWUVXVWJTKER8
     *
     * @return ViewModel|Response
     */
    public function redirectPosAction()
    {
        if (!$this->userService->checkUserRoleAccessMin(3) && !$this->userService->checkUserRoleAccess(101)) {
            return $this->redirect()->toRoute('bitkorn_login');
        }
        $viewModel = new ViewModel();
        $this->layout('layout/admin');

        $paymentId = $this->params()->fromQuery('paymentId');
        $token = $this->params()->fromQuery('token');
        $payerId = $this->params()->fromQuery('PayerID');

        if (empty($paymentId) || empty($token) || empty($payerId)) {
            $this->getResponse()->setStatusCode(\Laminas\Http\PhpEnvironment\Response::STATUS_CODE_500);
        }
        $shopBasket = $this->basketService->getShopBasketByPaymentNumber($paymentId);

        if (empty($shopBasket)) {
            $this->layout()->message = [
                'level' => 'error',
                'text' => 'Es ist ein Fehler aufgetreten. Bitte wenden sie sich mit folgender Payment-Number an den Support: ' . $paymentId
            ];
            return $viewModel;
        }

        $apiContext = $this->getApiContect();
        $payment = Payment::get($paymentId, $apiContext);
        $execution = new PaymentExecution();
        $execution->setPayerId($payerId);

        try {
            $resultPayment = $payment->execute($execution, $apiContext);
            /**
             * @var string $paymentState The state of the payment, authorization, or order transaction. The value is:
             *  - created. The transaction was successfully created.
             *  - approved. The buyer approved the transaction.
             *  - failed. The transaction request failed.
             */
            $paymentState = $resultPayment->getState();
            if ($paymentState == 'approved') {
                /**
                 * @todo ist der basket hier schon archieved???
                 */
                $this->basketFinalizeService->basketPayed($shopBasket['shop_basket_unique']);
            } else if ($paymentState == 'failed') {
                $this->basketService->setShopBasketStatusByPaymentNumber($paymentId, 'failed');
            }
            return $this->redirect()->toRoute($this->shopConfig['after_payment_redirect_route_pos'],
                [$this->shopConfig['after_payment_redirect_route_param_name'] => $shopBasket['shop_basket_unique']]);
        } catch (\Exception $ex) {
            $this->logger->err($ex->getMessage());
            $this->logger->err($ex->getFile() . ' at line: ' . $ex->getLine());
            $this->getResponse()->setStatusCode(\Laminas\Http\PhpEnvironment\Response::STATUS_CODE_500);
        }
        return $viewModel;
    }

    /**
     *
     * @return ApiContext
     */
    private function getApiContect()
    {
        $apiContext = new ApiContext(
            new OAuthTokenCredential(
                $this->shopPaypalConfig['client_id'], // ClientID
                $this->shopPaypalConfig['secret']      // ClientSecret
            )
        );
        $apiContext->setConfig(
            [
                'log.LogEnabled' => true,
                'log.FileName' => $this->shopPaypalConfig['logfile'],
                'log.LogLevel' => $this->shopPaypalConfig['loglevel'],
                'mode' => $this->shopPaypalConfig['mode']
            ]
        );
        return $apiContext;
    }

}
