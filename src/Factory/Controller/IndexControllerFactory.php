<?php

namespace Bitkorn\ShopPaypal\Factory\Controller;

use Bitkorn\Shop\Service\Basket\BasketFinalizeService;
use Bitkorn\Shop\Service\Basket\BasketService;
use Bitkorn\Shop\Service\ShopService;
use Bitkorn\ShopPaypal\Controller\IndexController;
use Bitkorn\User\Service\UserService;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class IndexControllerFactory implements FactoryInterface
{
	/**
	 * Create an object
	 *
	 * @param ContainerInterface $container
	 * @param string $requestedName
	 * @param null|array $options
	 * @return object
	 * @throws ServiceNotFoundException if unable to resolve the service
	 * @throws ServiceNotCreatedException if an exception is raised when creating a service
	 */
	public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
	{
		$controller = new IndexController();
		$controller->setLogger($container->get('logger'));
        $controller->setUserService($container->get(UserService::class));
        $controller->setShopConfig($container->get('config')['bitkorn_shop']);
        $controller->setShopPaypalConfig($container->get('config')['bitkorn_shop_paypal']);
        $controller->setShopService($container->get(ShopService::class));
        $controller->setBasketService($container->get(BasketService::class));
        $controller->setBasketFinalizeService($container->get(BasketFinalizeService::class));
		return $controller;
	}
}
