<?php

namespace Bitkorn\ShopPaypal;

use Bitkorn\ShopPaypal\Controller\IndexController;
use Bitkorn\ShopPaypal\Factory\Controller\IndexControllerFactory;
use Bitkorn\ShopPaypal\Factory\View\Helper\PaymentServiceBrandFactory;
use Bitkorn\ShopPaypal\Factory\View\Helper\PaymentServicePosBrandFactory;
use Bitkorn\ShopPaypal\View\Helper\PaymentServiceBrand;
use Bitkorn\ShopPaypal\View\Helper\PaymentServicePosBrand;
use Laminas\Router\Http\Literal;
use Laminas\Router\Http\Segment;

return [
    'router' => [
        'routes' => [
            'payment_paypal_start' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/paypal',
                    'defaults' => [
                        'controller' => IndexController::class,
                        'action' => 'index',
                    ],
                ],
            ],
            'paypal_redirect' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/paypal-redirect[/:success]',
                    'constraints' => [
                        'success' => '[0-1]*'
                    ],
                    'defaults' => [
                        'controller' => IndexController::class,
                        'action' => 'redirect',
                    ],
                ],
            ],
            'payment_paypal_pos' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/paypal-pos',
                    'defaults' => [
                        'controller' => IndexController::class,
                        'action' => 'indexPos',
                    ],
                ],
            ],
            'paypal_redirect_pos' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/paypal-redirect-pos[/:success]',
                    'constraints' => [
                        'success' => '[0-1]*'
                    ],
                    'defaults' => [
                        'controller' => IndexController::class,
                        'action' => 'redirectPos',
                    ],
                ],
            ],
        ],
    ],
    'controllers' => [
        'factories' => [
            IndexController::class => IndexControllerFactory::class,
        ],
    ],
    'service_manager' => [
        'factories' => [
        ],
        'invokables' => [
        ],
    ],
    'view_helpers' => [
        'aliases' => [
            'paymentBrandPaypal' => PaymentServiceBrand::class,
            'paymentPosBrandPaypal' => PaymentServicePosBrand::class,
        ],
        'factories' => [
            PaymentServiceBrand::class => PaymentServiceBrandFactory::class,
            PaymentServicePosBrand::class => PaymentServicePosBrandFactory::class,
        ],
        'invokables' => [
        ],
    ],
    'view_manager' => [
        'template_map' => [
            'template/paymentServiceBrandPaypal' => __DIR__ . '/../view/template/paymentServiceBrand.phtml',
            'template/paymentServicePosBrandPaypal' => __DIR__ . '/../view/template/paymentServicePosBrand.phtml',
        ],
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
    ],
    'bitkorn_shop_paypal' => [
        'mode' => 'sandbox', // sandbox | live
        'loglevel' => 'DEBUG', // DEBUG | FINE
        'client_id' => 'AUFx8sxwtkScNGi0eYGp36Z7Wvj_qWPsMB2hkV_wDmJ_0DbXGnCfm_Xh4H_EXgrSjSPmuxREJH7ZkvN2',
        'secret' => 'EFzjdSzVK6hr8viTybzKQXE5Fw74Uw9kaBhAwpfyfzXBP2Zt94AeWEmViuyYU96ZxtY0PObM8xltjsaG',
        'logfile' => __DIR__ . '/../../../../data/log/app.log'
    ]
];
