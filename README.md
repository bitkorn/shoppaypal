[DEPRECATED](https://github.com/paypal/PayPal-PHP-SDK#direct-credit-card-support)

## Install
- depending on bitkorn\shop
- depending PayPal API
    - composer require "paypal/rest-api-sdk-php:*"
    - or direct from [github](https://github.com/paypal/PayPal-PHP-SDK/releases)
- sudo chmod 666 /pathtomodule/BitkornShopPaypal/data/log/paypal.log

- /BitkornShopPaypal/Controller/IndexController()->getApiContect()
    - mode => sandbox | live


# help

[developer.paypal.com/docs/api/overview](https://developer.paypal.com/docs/api/overview/)

[github.com/paypal/PayPal-PHP-SDK/wiki](https://github.com/paypal/PayPal-PHP-SDK/wiki)

[paypal.github.io/PayPal-PHP-SDK](https://paypal.github.io/PayPal-PHP-SDK/)
